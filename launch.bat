@echo off
title 2009scape

set sql=%~dp0\db\bin\
set data=%~dp0\data\
set db=%~dp0\db\
set home=%~dp0

:start
cls
echo 1. Launch
echo 2. Initialize database
echo 3. Reset database
echo 4. Grant a player admin rights
echo 5. Grant a player mod rights
echo 6. Exit
echo:
set /p action=Pick number:
if /i "%action%"=="1" goto run
if /i "%action%"=="2" goto initDB
if /i "%action%"=="3" goto reset
if /i "%action%"=="4" goto role-admin
if /i "%action%"=="5" goto role-mod
if /i "%action%"=="6" goto exit
set /p action=""
goto start


:role-admin
taskkill /f /im mysqld*
taskkill /f /im java*
cls
cd "%db%"
call start /min "" "%sql%mysqld.exe" --console --skip-grant-tables --lc-messages-dir="%cd%\share" --datadir="%cd%\data"
ping localhost -n 4 > nul
cls
set /p username=Please enter the user to make an admin:
call "%sql%mysql.exe" -uroot -e "USE global; UPDATE `members` SET `rights` = '2' WHERE `members`.`username` = '%username%';"
ping localhost -n 3 > nul
echo:
echo %username% is now an Administrator!
echo:
pause
goto start


:role-mod
taskkill /f /im mysqld*
taskkill /f /im java*
cls
cd "%db%"
call start /min "" "%sql%mysqld.exe" --console --skip-grant-tables --lc-messages-dir="%cd%\share" --datadir="%cd%\data"
ping localhost -n 4 > nul
cls
set /p username=Please enter the user to make an mod:
call "%sql%mysql.exe" -uroot -e "USE global; UPDATE `members` SET `rights` = '1' WHERE `members`.`username` = '%username%';"
ping localhost -n 3 > nul
echo:
echo %username% is now an Moderator!
echo:
pause
goto start


:exit
taskkill /f /im Java*
taskkill /f /im mysqld*
exit


:run
cls
cd "%db%"
start /b "db" "%sql%mysqld.exe" --console --skip-grant-tables --lc-messages-dir="%cd%\share" --datadir="%cd%\data"
ping localhost -n 6 > nul
cls
cd "%home%/game/"
start /b %~dp0\jre\bin\java.exe -Xmx2G -Xms1G -jar server.jar
ping localhost -n 10 > nul
cd "%home%/game/"
start /b /w %~dp0\jre\bin\java.exe -Xmx1G -Xms512m -jar client.jar
echo:
goto start


:initDB
cls
pushd %~dp0\db\bin
mysql_install_db -d ../data
start /B mysqld.exe
timeout 2 > NUL
echo create database global; use global; source ../template/global.sql; | mysql -u root
taskkill /im mysqld.exe /f
pause
goto start


:reset
taskkill /f /im Java*
taskkill /f /im mysqld*
cls
echo:
echo Are you ABSOLUTELY SURE that you want to reset all game dbs?
echo:
echo To confirm the db reset, type yes and press enter.
echo:
set /p confirmwipe=""
echo:
if /i "%confirmwipe%"=="yes" goto wipe
echo Error! %confirmwipe% is not a valid option.
pause
goto start


:wipe
cls
rmdir /S /Q "%db%data"
goto start
